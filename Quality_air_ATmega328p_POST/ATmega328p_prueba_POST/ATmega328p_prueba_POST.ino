/*
LUIS ENRIQUE GARCÍA HERNÁNDEZ (Rev 1.0)
David González Gamboa (Rev 2.0)
Estación de condiciones climaticas via WiFi con ATmega328p.
*/

// Numero de variables y funciones
//#define NUMBER_VARIABLES 8
//#define NUMBER_FUNCTIONS 2
//Librerias
#include <Arduino.h>
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include <utility/debug.h>
#include <DHT.h>
#include <math.h>

/*
MQ-135 >>Dioxido de carbono = field3        (entrada analógica 0)
MQ-7 >>Monoxido de carbono = field4         (entrada analógica 1)
MQ-131 >>Ozono = field5                     (entrada analógica 2)
Dioxido de azufre = PENDIENTE   	    (entrada analógica 3)
Dioxido de nitrogeno = PENDIENTE            (entrada analógica 4)
Luz ambiental = field8                      (entrada analógica 5)
DHT-11 >>Temperatura = field1               (entrada digital 6)
DHT-11 >>Humedad = field2                   (emtrada digital 6)
Particulas suspendidas = field6             (entrada digital 7)
*/

// Declaracion de pines para el WiFi shield
#define ADAFRUIT_CC3000_IRQ   3
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIVIDER); //velocidad del reloj

//Datos de la conexiona la red
#define WLAN_SSID       "SIGPOTMEGA"  //Nombre de la red a conectar   "SIGPOT"      "Xperia"
#define WLAN_PASS       "4A61F8DF47D69454" //contraseña de la red         "9648BDEA45"    "enrique2016"
//Tipos de seguridad WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA, WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2       

// pines del sensor DTH11
#define DHTPIN 6
#define DHTTYPE DHT22

// Crear instancias para el CC3000 y DHT
DHT dht(DHTPIN, DHTTYPE);

#define IDLE_TIMEOUT_MS  3000      //Tiempo de espera en milisegundos sin datos recibidos antes de cerrar la conexi�n.

// Pagina a conectar
#define  WEBSITE "Servidor SIGPOT"  //Pagina a la que se desea conectar

// Variables para ser expuestas en el API
float temperatura;
float humedad;
float CO2_pin = A0;
float CO_pin = A1;
float O3_pin = A2;
float SO2_pin = A3;
float NO2_pin = A4;
float Luxometro_pin = A5;

//Iniciar las variables en cero
float CO2 = 0;
float CO = 0;
float O3 = 0;
float particulas = 0;
float luz = 0;
float SO2 = 0;
float NO2 = 0;

//*******************************************************************************************************************************
/*DATOS IMPORTANTES PARA EL SENSOR DE PARTICULAS
         *0.01 ft^3 (pie cubico)= 0.0002831685 m^3 (metros cubicos)
         *Para convertir una concentración expresada en unidades de ppb a una concentración expresada en unidades de g/m:
          multiplicar la concentración (en ppb) por el peso molecular del contaminante (en gramos/moles) y dividir por 24,45.
         *1 ppm = 1000 ppb
 */

int particulas_pin = 7;                        //Pin de entrada de datos del sensor
unsigned long duration;                        //Duracion
unsigned long starttime;                       //Tiempo de inicio
unsigned long sampletime_ms = 5000;            //Muestra en 30 segundos
unsigned long lowpulseoccupancy = 0;           //Ocupacion del pulso en bajo
float ratio = 0;                               //Proporsion o relacion
float concentration = 0;                       //Concentracion

//*******************************************************************************************************************************

uint32_t ip; //Declara una variable ip para la direccion de la pagina a ingresar

//Declara la variable spData, esta guardara los datos a enviar
String spData;

void setup(void){
  Serial.begin(57600);
  Serial.println(F("ESTACION DE DETECCION DE CALIDAD DEL AIRE SIGPOT IoT\n"));

  Serial.print("Free RAM: ");
  Serial.println(getFreeRam(), DEC);

  // Inicializacion del sensor DHT
  dht.begin();

  //Direccion IP de la pagina
  //ip = 198976461 ;                   //ip de ThingSpeak(189.194.60.13)=> convertida a hexadecimal (BD.C2.3C.D)=> convertida a decimal (3093993877)
  ip= 3232235523;	//192.168.0.3;				C0 A8 00 03

  //Sensor de particulas
  pinMode(particulas_pin,INPUT);
}

void loop(void)
{
  // Inicializacion del modulo
  Serial.println(F("\nIniciando..."));
  if (!cc3000.begin())
  {
    Serial.println(F("No se puede iniciar, Checar conexion"));
    while(1);
    Serial.flush(); exit(0);
  }

  Serial.print(F("\nIntentando conectar a la red "));
  Serial.println(WLAN_SSID);

  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Error al intentar conectar!"));
    while(1);
    Serial.flush(); exit(0);
  }

  Serial.println(F("Conectado!"));

  /* Espera para completar el DHCP */
  Serial.println(F("Solicitud del DHCP"));
  while (!cc3000.checkDHCP())
  {
    delay(100); // ToDo: Insert a DHCP timeout!
  }

  /* Despliega la direccion IP, DNS, Gateway, etc. */
  while (! displayConnectionDetails()) {
    delay(1000);
  }
  //Llamada a las funciones
  int lectura = 0;

  Serial.println();
  Serial.println(F("Capturando variablelor de temperatura y humedad..."));
  float tempe, hume;
  tempe = Medicion_temperatura();
  hume = Medicion_humedad();
  Serial.println(F("Listo"));

  Serial.println(F("Capturando valor de dioxido de carbono..."));
  float diox;
  diox = Medicion_dioxido_carbono();
  Serial.print(F("Valor analogico= "));
  lectura = (analogRead(CO2_pin));
  Serial.println(lectura);
  Serial.println(F("Listo"));

  Serial.println(F("Capturando valor de monoxido de carbono..."));
  float monox;
  monox = Medicion_monoxido_carbono();
  Serial.print(F("Valor analogico= "));
  lectura = (analogRead(CO_pin));
  Serial.println(lectura);
  Serial.println(F("Listo"));

  Serial.println(F("Capturando valor de ozono..."));
  float oz;
  oz = Medicion_ozono();
  Serial.print(F("Valor analogico= "));
  lectura = (analogRead(O3_pin));
  Serial.println(lectura);
  Serial.println(F("Listo"));

  Serial.println(F("Capturando valor de particulas suspendidas..."));
  float parti;
  parti = Medicion_particulas();
  Serial.println(F("Listo"));

  Serial.println(F("Capturando valor de luz ambiental..."));
  float lam;
  lam = Medicion_luz_ambiental();
  Serial.print(F("Valor analogico= "));
  lectura = (analogRead(Luxometro_pin));
  Serial.println(lectura);
  Serial.println(F("Listo"));
  
  //covertir a String los datos sensados
  String hum = String(float(hume));
  String temp = String(float(tempe));
  String co2 = String(float(diox));
  String co = String(float(monox));
  String o3 = String(float(oz));
  String part = String(float(parti));
  String lus = String(float(lam));
  //Formateo de datos para envio 
  spData = "field1=" + hum + "+&field2=" + temp + "+&field3=" + co2 + "+&field4=" + co + "+&field5=" + o3 + "+&field6=" + part + "+&field7"= lus;

  // Try looking up the website's IP address
  Serial.println();
  Serial.print(WEBSITE);
  Serial.print(F(" -> "));
  cc3000.printIPdotsRev(ip);
  delay(1000);

  //****************************************************************************************************************************

  Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);

  Serial.println();
  Serial.println(F("Conectando a la pagina... "));

  if (www.connected()) {
    www.print("POST /sensor/airspot/update.php HTTP/1.1\r\n");
    www.print("Host: 192.16.0.3\n");
    www.print("Content-Type: application/x-www-form-urlencoded\n");
    www.print("Content-Length: ");
    www.print(spData.length());
    www.print("\n\n");
    www.println(spData);
    Serial.println(F("Comunicacion realizada"));
  }
  else {
    Serial.println(F("Conexion fallida"));
    return;
  }

  Serial.println(F("-------------------------------------"));

  // Read data until either the connection is closed, or the idle timeout is reached.

      unsigned long lastRead = millis();
      while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS))
      {
      while (www.available())
      {
      char c = www.read();
      Serial.print(c);
      lastRead = millis();
      }
      }

  www.close();

  //Llamando a

  Serial.println(F("---------------VALORES DE LAS VARIABLES----------------------"));
  Serial.println();

  Serial.print(F("Temperatura="));
  Serial.print(temperatura);
  Serial.println(F(" C"));

  Serial.print(F("Humedad="));
  Serial.print(humedad);
  Serial.println(F(" RH"));

  Serial.print(F("Dioxido de Carbono="));
  Serial.print(CO2);
  Serial.println(F(" ppm"));

  Serial.print(F("Monoxido de Carbono="));
  Serial.print(CO);
  Serial.println(F(" ppm"));

  Serial.print(F("Ozono="));
  Serial.print(O3);
  Serial.println(F(" ppm"));

  Serial.print(F("Particulas suspendidas="));
  Serial.print(particulas);
  Serial.println(F(" particulas/m3"));

  Serial.print(F("Luz ambiental="));
  Serial.print(luz);
  Serial.println(F(" luxes"));

  Serial.print(spData);
  
  Serial.println(F("----------------------------------------------------------------"));
  Serial.println();

  Serial.println(F("\nDesconectando..."));
  cc3000.disconnect();   //Desconecta de la red
  Serial.println(F("Conexion deshabilitada..."));

  Serial.println(F("\nEjecutando tiempo de espera..."));
  delay(30000);         //Retardo de 16 seg para volver a acualizar(300000=5 minutos)
  Serial.println(F("Tiempo de espera finalizada..."));

}

<<<<<<< magnustesting
=======

void convertir_aString(char* c, char* cadena){

	strncpy(c, cadena, 200);
	c[200] = '\0';    //Cierre de cadena
	cadena2= c;
}

>>>>>>> Nada
//****************************************************************************************************************************
//*******************************************MEDICION DE SENSORES*************************************************************
//****************************************************************************************************************************

float Medicion_temperatura(){
  temperatura = (uint8_t)dht.readTemperature();     //Lee el valor de la temperatura obtenido por el sensor
  return temperatura;
}

float Medicion_humedad (){
  humedad = (uint8_t)dht.readHumidity();          //Lee el valor de la humedad obtenida por el sensor
  return humedad;
}


float Medicion_dioxido_carbono() {
  float a = 116.59;
  float b = -2.77;
  int RL = 10000;                               //Resistencia de carga en ohms
  float RO = 71175;  //112950                   //Resistencia del sensor en ohms
  int Vc = 5;                                   //Voltaje de alimentacion en volts
  float captura = analogRead(CO2_pin);          //Lee el valor de el CO2 obtenida por el sensor
  float VRL = (captura*Vc)/1023;                //Convierte lo capturado a voltaje
  float RS = ((Vc-VRL)/VRL)*RL;
  CO2 = calcular_ppm(a,b,RS,RO);
  return CO2;
}

float Medicion_monoxido_carbono() {
  float a = 103.88;
  float b = -1.499;
  int RL = 10000;                               //Resistencia de carga en ohms
  float RO = 15025;     //10650                 //Resistencia del sensor en ohms
  int Vc = 5;                                   //Voltaje de alimentacion en volts
  float captura = analogRead(CO_pin);          //Lee el valor de el CO2 obtenida por el sensor
  float VRL = (captura*Vc)/1023;                //Convierte lo capturado a voltaje
  float RS = ((Vc-VRL)/VRL)*RL;
  CO = calcular_ppm(a,b,RS,RO);
  return CO;
}

float Medicion_ozono() {
  float a = 25.157;
  float b = -1.121;
  int RL = 10000;                               //Resistencia de carga en ohms
  float RO = 46100;    //507450                 //Resistencia del sensor en ohms
  int Vc = 5;                                   //Voltaje de alimentacion en volts
  float captura = analogRead(O3_pin);          //Lee el valor de el CO2 obtenida por el sensor
  float VRL = (captura*Vc)/1023;                //Convierte lo capturado a voltaje
  float RS = ((Vc-VRL)/VRL)*RL;
  O3 = calcular_ppm(a,b,RS,RO);
  return O3;
}

float Medicion_particulas(){

  starttime = millis();                        //Obtener el tiempo actual transcurrido

  while (millis()-starttime < sampletime_ms){
  duration = pulseIn(particulas_pin, LOW);                      //Guarda en la variable la duracion del pulso en bajo del pin de entrada
  Serial.print(F("DURACION DE PULSO EN BAJO= "));
  Serial.println(duration);
  lowpulseoccupancy = lowpulseoccupancy+duration;    //Suma todos los valores de la duracion del pulso en bajo
  }
    Serial.print(F("TIEMPO DE OCUPACION DE PULSO EN BAJO= "));
    Serial.println(lowpulseoccupancy);
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);    // Porsentaje de integracion 0=>100
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62;   // utilizando la ecuacion de la curva de hoja de especificaciones
    particulas = concentration / 0.0002831685;     //Convierte la concentracion de pies cubicos a metros cubicos
    lowpulseoccupancy = 0;
    return particulas;
}

float Medicion_luz_ambiental(){

  int Vin=5;
  int RL=10000;
  float a = 970.49;          //Define una costante para el calculo de lux
  //a=255.2;
  float b = -1.664;          //Define una costante para el calculo de lux
  //b=-1.563;
  float vRL;   //voltaje en la resistencia de carga
  int captura = analogRead(Luxometro_pin);              //Lee y guarda el valor de la entrada analogica
  vRL = (float)captura*5/1023;            //Calcula el voltaje de la resistencia de carga
  float LDR = (float)((Vin-vRL)/(vRL/RL))/1000;      //Calcula el valor de la resistencia LDR en kohms
  luz = a*pow(LDR,b);
  return luz;
  /*float RS;                                      //Resistencia del sensor en Kilo ohms
  int captura = analogRead(sensor5);         //Lee la entrada
  RS=(float)(1023-captura)*10/captura;   //Resistencia del sensor
  int RO=1;                                      //Valor asignado para dividir en la formula entre 1
  float a=300.05;                                //constante de la ecuacion de la grafica de comportamieto
  float b=-1.3;                                  //constante de la ecuacion de la grafica de comportamieto
  luz = calcular_ppm(a,b,RS,RO);                 //Calcula la cantidad de luz emitida por el sensor en luxes
*/
}

//****************************************************************************************************************************
//Subrutina para calcular la concentracion de los gases
float calcular_ppm (float a, float b, float RS, float RO){
float ppm=0;
ppm = (a*(pow((RS/RO),b)));                     //Consentracion del gas en PPM
return(ppm);
}
//****************************************************************************************************************************
void listSSIDResults(void)
{
  uint8_t valid, rssi, sec, index;
  char ssidname[33];

  //index = cc3000.startSSIDscan();

  Serial.print(F("Networks found: ")); Serial.println(index);
  Serial.println(F("================================================"));

  while (index) {
    index--;

    valid = cc3000.getNextSSID(&rssi, &sec, ssidname);

    Serial.print(F("SSID Name    : ")); Serial.print(ssidname);
    Serial.println();
    Serial.print(F("RSSI         : "));
    Serial.println(rssi);
    Serial.print(F("Security Mode: "));
    Serial.println(sec);
    Serial.println();
  }
  Serial.println(F("================================================"));

  cc3000.stopSSIDscan();
}

/*****************************************************************************************************************************/

bool displayConnectionDetails(void)
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;

  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
    return true;
  }
}
