<?php

namespace airspot\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use airspot\Http\Requests;

class Recepcion extends Controller
{
	public function obtenerDatos(Request $request){
		if($request->isMethod('post')){
			$data=$request->all();
			if($data){
				if($request->has('z') && count($data)==7){
					// En caso de que esté partida la peticion
					// Verificacion de los datos e indices para determinar si están establecidos
					if(self::verificarDatos($data,self::arrayValores($data['z']))){
						// Coincidieron datos con los requeridos en la peticion
						// Obtener el identificador de la estacion
						$estacion=self::obtenerEstacionTemp($data['_token']);
						if(isset($estacion) && $data['z']==1){
							// Primera partde del envio de las estaciones
							// Solo se inserta los datos de 
							self::insertarDatos($data,$estacion->kid,self::arrayKeys($data['z']));
							return abort(404);
						}elseif (isset($estacion) && $data['z']==2) {
							// Segunda parte del envio de las estaciones
							self::insertarDatos($data,$estacion->kid,self::arrayKeys($data['z']));
							self::actualizarDatos($estacion->kid);
						}else{
							return abort(404);
						}
					}else{
						return abort(404);
					}
				}elseif(self::verificarDatos($data,self::arrayValores()) && count($data)==11){
					return self::verificarExistencia($data);
				}else{
					return abort(404);
				}
			}else{
				return abort(404);
			}
		}else{
			return abort(404);
		}
	}


	// Tentativamente se tiene que enviar un arreglo con los valores 
	private function verificarDatos($datos,$arreglo){
		foreach(array_keys($datos) as $dato){
			if(!is_numeric($datos[$dato])){
				$datos[$dato]=0;
			}
			if(!array_key_exists($dato,$arreglo)){
				return false;
			}
		}
		return true;
	}

	private function verificarExistencia($datos){
		$id=DB::table('estaciones')->select('id')->where('kid','=',$datos['nomest'])->first();
		if($id){
			$l=DB::table('lecturas')->select('id','temperatura','humedad','co2','co','o3','pm2_5','so2','no2','luz','pm10','created_at','updated_at')->where('key_id','=',$id->id)->first();
			if($l){
				return 'Éxito';
			}else{
				return abort(404);
			}
		}else{
			return abort(404);
		}
	}

	private function insertData($l){
		DB::table('historicos')->insert(['key_id'=>$l->id,'temperatura'=>$l->temperatura,'humedad'=>$l->humedad,'co2'=>$l->co2,'co'=>$l->co,'o3'=>$l->o3,'pm2_5'=>$l->pm2_5,'so2'=>$l->so2,'no2'=>$l->no2,'luz'=>$l->luz,'pm10'=>$l->pm10]);
		DB::table('estaciones')->where('id','=',$id->id)->update(['temperatura'=>$datos['f0'],'humedad'=>$datos['f1'],'co2'=>$datos['f2'],'co'=>$datos['f3'],'o3'=>$datos['f4'],'pm2_5'=>$datos['f5'],'so2'=>$datos['f6'],'luz'=>$datos['f7'],'no2'=>$datos['f8'],'pm10'=>$datos['f9'],'created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()]);
		return true;
	}


	//	Funcion que devuelve un arreglo dependiendo de la bandera que se recibe
	private function arrayValores($a=0){
		if($a==1 || $a==2){
			return array('_token'=>'_token','z'=>'z','a'=>'a','b'=>'b','c'=>'c','d'=>'d','e'=>'e');
		}else{
			return array('_token'=>'_token','z'=>'z','a'=>'a','b'=>'b','c'=>'c','d'=>'d','e'=>'e','f'=>'f','g'=>'g','h'=>'h','i'=>'i','j'=>'j');
		}
	}

	private function arrayKeys($a=0){
		if ($a==1) {
			return array('temperatura','humedad','co2','co','o3');
		}elseif($a==2){
			return array('pm2_5','so2','no2','luz','pm10');
		}else{
			return abort(404);
		}
	}

	private function obtenerEstacionTemp($token){
		return DB::table('templec')->select('kid')->where('temptoken','like',$token)->first();
	}

	private function insertarDatos($datos,$estacion,$arreglo){
		$kid=DB::table('templec')->select('kid')->where('kid','=',$estacion)->first();
		DB::table('templec')->where('kid','=',$kid->kid)->update([$arreglo[0]=>$datos['a'],$arreglo[1]=>$datos['b'],$arreglo[2]=>$datos['c'],$arreglo[3]=>$datos['d'],$arreglo[4]=>$datos['e']]);
		return true;
	}

	private function actualizarDatos($estacion_id){
		$newlec=DB::table('templec')->select('temperatura','humedad','co2','co','o3','pm2_5','so2','no2','luz','pm10')->where('kid','=',$estacion_id)->first();
		$oldlec=DB::table('lecturas')->select('key_id','temperatura','humedad','co2','co','o3','pm2_5','so2','no2','luz','pm10','created_at','updated_at')->where('key_id','=',$estacion_id)->first();
		if(!is_null($oldlec)==1){
			// si existe la lectura anterior en la tabla lecturas
			// se actualiza los datos de la tabla templec a lecturas
			DB::table('historicos')->insert(['key_id'=>$estacion_id,'temperatura'=>$oldlec->temperatura,'humedad'=>$oldlec->humedad,'co2'=>$oldlec->co2,'co'=>$oldlec->co,'o3'=>$oldlec->o3,'pm2_5'=>$oldlec->pm2_5,'so2'=>$oldlec->so2,'no2'=>$oldlec->no2,'luz'=>$oldlec->luz,'pm10'=>$oldlec->pm10,'created_at'=>$oldlec->created_at,'updated_at'=>$oldlec->updated_at]);
			//falta actualizar los datos de la tabla templec a lecturas
			DB::table('lecturas')->where('key_id','=',$estacion_id)->update(['temperatura'=>$newlec->temperatura,'humedad'=>$newlec->humedad,'co2'=>$newlec->co2,'co'=>$newlec->co,'o3'=>$newlec->o3,'pm2_5'=>$newlec->pm2_5,'so2'=>$newlec->so2,'no2'=>$newlec->no2,'luz'=>$newlec->luz,'pm10'=>$newlec->pm10,'created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()]);
			DB::table('templec')->where('kid','=',$estacion_id)->delete();
			return true;
		}else{
			// si no existe lectura anterior en la tabla lecturas
			// se insertan los datos de la tabla templec a lecturas
			DB::table('lecturas')->insert(['key_id'=>$estacion_id,'temperatura'=>$newlec->temperatura,'humedad'=>$newlec->humedad,'co2'=>$newlec->co2,'co'=>$newlec->co,'o3'=>$newlec->o3,'pm2_5'=>$newlec->pm2_5,'so2'=>$newlec->so2,'no2'=>$newlec->no2,'luz'=>$newlec->luz,'pm10'=>$newlec->pm10,'created_at'=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()]);

			DB::table('templec')->where('kid','=',$estacion_id)->delete();
			return true;
		}
	}
 
	public function keyData(Request $request){
		if($request->has('llave') && $request->isMethod('get')){
			$bool=DB::table('estaciones')->select('id')->where('kid',$request->input('llave'))->first();
			if($bool){
				$token=csrf_token();
				DB::table('templec')->insert(['kid'=>$bool->id,'temptoken'=>$token]);
				return (new \Illuminate\Http\Response)->setStatusCode(200, '|'.$token.'*');
			}else{
				return abort(404);
			}
		}else{
			return abort(404);
		}
	}

	public function eliminarTemp(Request $req){
		if($req->has('n') && $req->has('k')){
			$pet=$req->all();
			$est=self::buscarEstacion($pet['n']);
			if(!is_null($est->est)){
				if(self::compareKeys($est->est,$pet['k'])){
					DB::table('templec')->where('kid','=',$est->ident)->delete();
					return abort(503);
				}else{
					return abort(404);
				}
			}else{
				return abort(404);
			}
		}else{
			return abort(404);
		}
	}

	private function buscarEstacion($est){
		return DB::table('estaciones')->select('nombre as est','id as ident')->where('nombre','=',$est)->first();
	}

	private function compareKeys($est,$key){
		return md5($est.'del') == $key ? true : false;
	}
}