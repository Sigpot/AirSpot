<?php

namespace airspot\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use airspot\Http\Requests;

class Date extends Controller
{
	public function HistData(Request $request){
		if($request->has('date') && $request->has('n')){
			if(is_numeric($request->input('date'))){
				$datos=$request->all();
				$date=date('Ymd',$datos['date']/1000);
				$ne=DB::table('estaciones')->select('id as est')->where('nombre','like',$datos['n'])->get();
				if($ne[0]->est){
					return DB::table('historicos')->where([['key_id','=',$ne[0]->est],['created_at','>=',$date],['created_at','<',date('Y-m-d',strtotime($date.'+1 day'))]])->get();
				}else{
					return abort(404);
				}
			}else{
				return abort(404);
			}
		}else{
			return abort(404);
		}
	}

	public function DateWData(Request $req){
		if(!$req->has('e')){
			abort(404);
		}else{
			$e=DB::table('estaciones')->select('id')->where('nombre','like',$req->e)->first();
			if(!$e){
				return abort(404);
			}else{
				return DB::table('historicos')->select(DB::raw("to_char(created_at, 'YYYY,MM,DD') as date"))->where('key_id','=',$e->id)->groupBy('date')->get();	
			}
		}
	}
}
