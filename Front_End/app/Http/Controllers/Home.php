<?php

namespace airspot\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use JavaScript;

use airspot\Http\Requests;
use airspot\Estacion;
use airspot\Key;

class Home extends Controller
{
	public function index(){

		$estaciones = DB::table('estaciones')->select('id','nombre','lon','lat')->get();
		$lec = array();
		foreach ($estaciones as $estacion) {
			$lecturas=DB::table('lecturas')->select('temperatura','temperatura','humedad','co2','co','o3','pm2_5','so2','no2','luz','pm10')->where('key_id','=',$estacion->id)->first();
			$lec[$estacion->id] = array_merge((array) $estacion,(array) $lecturas);
		}
		Javascript::put(['estaciones' => $lec]);
		return view('principal');
	}
}
