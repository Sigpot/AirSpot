<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Home@index');

Route::post('/datos','Recepcion@obtenerDatos');

Route::get('/historico', 'Date@HistData');

Route::get('/dcd', 'Date@DateWData');

Route::auth();

Route::get('/kdata', 'Recepcion@keyData');

Route::get('/admin', 'HomeController@index');

Route::delete('/tempdel', 'Recepcion@eliminarTemp');