<?php

namespace airspot;

use Illuminate\Database\Eloquent\Model;

class Estacion extends Model
{
    // Nombre de la tabla
    protected $table="lecturas";

    // Campos llenables
    protected $fillable=['temperatura','humedad','co2','co','o3','pm_2_5','so2','pm_10','no2','luz'];

    // Relaciones
    public function key(){
    	return $this->hasOne('airspot\Key');
    }
}