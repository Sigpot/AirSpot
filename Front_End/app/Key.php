<?php

namespace airspot;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    // Nombre de la tabla
    protected $table="estaciones";

    // Campos llenables
    protected $fillable=['kid','lon','lat'];

    // Campos Ocultos
    protected $hidden=['created_at','updated_at','kid'];

    protected $primaryKey = 'kid';

    public $incrementing = false;

    // Relaciones
    public function estacion(){
    	return $this->hasOne('airspot\Estacion');
    }
}