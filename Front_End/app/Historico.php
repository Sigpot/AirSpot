<?php

namespace airspot;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    // Nombre de la tabla
    protected $table='historicos';

    // Campos llenables
    protected $fillable=['temperatura','humedad','co2','co','o3','pm_2_5','so2','pm_10','no2','luz'];

    protected $hidden['created_at','updated_at'];
}
