<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableHistoricos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicos', function (Blueprint $table) {
            $table->integer('key_id');
            $table->string('temperatura');
            $table->string('humedad');
            $table->string('co2');
            $table->string('co');
            $table->string('o3');
            $table->string('pm2_5');
            $table->string('so2');
            $table->string('no2');
            $table->string('luz');
            $table->string('pm10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historicos');
    }
}
