<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('lecturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('key_id')->unique()->unsigned();
            $table->string('temperatura');
            $table->string('humedad');
            $table->string('co2');
            $table->string('co');
            $table->string('o3');
            $table->string('pm2_5');
            $table->string('so2');
            $table->string('no2');
            $table->string('luz');
            $table->string('pm10');
            $table->timestamps();
            $table->foreign('key_id')->references('id')->on('estaciones');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lecturas');
    }
}
