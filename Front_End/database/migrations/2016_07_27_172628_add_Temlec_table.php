<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemlecTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templec', function (Blueprint $table) {
            $table->integer('kid')->unique();
            $table->string('temptoken')->unique();
            $table->string('temperatura')->nullable();
            $table->string('humedad')->nullable();
            $table->string('co2')->nullable();
            $table->string('co')->nullable();
            $table->string('o3')->nullable();
            $table->string('pm2_5')->nullable();
            $table->string('so2')->nullable();
            $table->string('no2')->nullable();
            $table->string('luz')->nullable();
            $table->string('pm10')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('templec');
    }
}
