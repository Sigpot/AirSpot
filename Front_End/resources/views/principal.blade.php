@extends('public.main')
@section('titulo','Inicio de pagina')

@section('librerias_css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/chartist/chartist.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/chartist/chartist-plugin-tooltip.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/css/custom-chartist.css') }}">
@endsection

@section('ol_css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/ol3/ol.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/custom-ol3.css') }}">
	<link rel="stylesheet" media="(max-width: 500px)" href="{{ asset('plugins/css/mobile/custom-ol3-500.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/css/panel-bar.css') }}">
	<link rel="stylesheet" media="(max-width: 500px)" href="{{ asset('plugins/css/mobile/panel-bar-500.css') }}">
	<link rel="stylesheet" media="(max-width:850px) and (min-width:501px)" href="{{ asset('plugins/css/mobile/panel-bar-850.css') }}">
	<style type="text/css">
		html,body{
			height: 100%;
			font-family: 'Roboto', sans-serif;
		}
	</style>
@endsection

@section('overlayer_carga')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/overlayer.css') }}">
@endsection

@section('overlayer_content')
	<div id="overlayer" class="overlayer"></div>
@endsection

@section('mapa_div')
<div id="map"></div>
<div id="popup" class="ol-popup">
		<div id="popup-closer" class="ol-popup-closer">✖</div>
		<div id="popup-header" class="popup-header"></div>
		<div id="popup-content" class="popup-content"></div>
</div>
<div id="lectura" class="mostrar-datos contentminchart"></div>
@endsection

@section('librerias_extras')
<script src="{{ asset('plugins/chartist/chartist.min.js') }}"></script>
<script src="{{ asset('plugins/chartist/chartist-plugin-tooltip.js') }}"></script>
@endsection

@section('create_map')
<script src="{{ asset('plugins/ol3/ol-debug.js') }}"></script>
<script src="{{ asset('plugins/js/controls.js') }}"></script>
<script>
	var container = document.getElementById('popup');
	var header = document.getElementById('popup-header');
	var content = document.getElementById('popup-content');
	var closer = document.getElementById('popup-closer');
	var overlay = new ol.Overlay(({
		element: container,
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	}));
	closer.onclick = function() {
		closePanel($('#lectura'));
		closer.blur();
		return false;
	};
	$(function(){
	 	iniciar()
	 });

</script>
@endsection