<!DOCTYPE html>
<html lang="es">
<head>
	@if(is_null(Route::getCurrentRoute()->getPath()))
		<title>@yield('titulo','AirSpot') | SIGPOT S.A. de C.V.</title>
	@elseif(Route::getCurrentRoute()->getPath() == 'login')
		<title>@yield('titulo','Login  | AirSpot') | SIGPOT S.A. de C.V.</title>
	@else
		<title>@yield('titulo','Panel de Administracion | AirSpot') | SIGPOT S.A. de C.V.</title>
	@endif
	<meta name="keywords" content="CALIDAD,AIRE,SENSORES,SIGPOT,TEPIC,NAYARIT,ESTACIONES,ESTACION,MONITOREO">
	<meta name="description" content="AirSpot está conformada por un conjunto de estaciones de monitoreo de la calidad del aire, esta es la plataforma de visualizacion de lecturas de dicha estacion">
	<meta name="author" content="SIGPOT S.A. de C.V.">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="icon" href="{{ asset('images/favicon.svg') }}" sizes="16x16" type="image/svg+xml">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/materialize/css/materialize.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/custom-nav.css')  }}">
	@yield('librerias_css')
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Lemonada" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
	@yield('overlayer_carga')
	@yield('ol_css')
	@yield('estilo')
</head>
<body>
	@include('public.nav')
	@yield('mapa_div')
	<script type="text/javascript" src="{{ asset('plugins/jquery-2.1.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/materialize/js/materialize.min.js') }}"></script>
	@yield('librerias_extras')
	@yield('create_map')
</body>
</html>