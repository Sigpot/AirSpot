function setDataHist(response){
	window.historico=response;
	return historico;
}
function getDataHist(){
	return historico;
}

/*
 * iniciar()
 * Funcion que inicializa el mapa
 */
function iniciar(){
	var map = new ol.Map({
		view: new ol.View({
			projection: 'EPSG:4326',
			center: [-104.8998,21.5046],
			zoom: 1
		}),
		layers: [
			new ol.layer.Tile({
				source: new ol.source.XYZ({
					url:'http://{a-c}.tile.thunderforest.com/transport/{z}/{x}/{y}.png'
				})
			})
		],
		overlays: [overlay],
		controls:[new ol.control.Zoom()],
		target: 'map'
	});
	clickOnMap(map);
	zoomTo(map,-104.95,21.54,-104.81,21.44);

	addEstacionesMapa(map,estaciones);
}

/*
 *	zoomTo(map,x1,y1,x2,y2)
 *	Esta funcion hace zoom al extent 
 */
function zoomTo(map,x1,y1,x2,y2){
	var xy1=new ol.geom.Point(ol.proj.fromLonLat([x1,y1],'EPSG:4326')),xy2=new ol.geom.Point(ol.proj.fromLonLat([x2,y2],'EPSG:4326')),extent1=xy1.getExtent(),extent2=xy2.getExtent();
	var extent=new ol.extent.extend(extent1,extent2);
	try{
		map.getView().fit(extent,map.getSize());
	}catch(e){
		console.error(e);
	}
}

/*
 *	addEstacionesMapa(mapa,coordEstaciones)
 *	return void
 *	añade las estaciones al mapa
 */
function addEstacionesMapa(map,coordEstaciones){
	try{
		var features=[],feature;
		Object.keys(coordEstaciones).forEach(function(key){
			try{
				feature=new ol.Feature({
					name: "Estaciones",
					geometry: new ol.geom.Point(ol.proj.fromLonLat([coordEstaciones[key].lon,coordEstaciones[key].lat],'EPSG:4326')),
				});
				if(coordEstaciones[key].temperatura){
					var colorMarcador=obtenerColor(coordEstaciones[key]);
				}else{
					var colorMarcador='9a9a9a';
				}
				featureSetProperties(feature,coordEstaciones[key]);
				var estilo=markerStyle(colorMarcador);
				feature.setStyle(estilo);
				features.push(feature);
			}catch(e){
				console.error('Algo falló. Éste es el error: '+e);
			}
		});
		var estacionesSource=new ol.source.Vector({features:features});
		var estacionesVector=new ol.layer.Vector({source:estacionesSource});
		map.addLayer(estacionesVector);
	}catch(e){
		console.error('Error añadiendo las estaciones al mapa: '+e);
	}
}

/*
 *	Funcion que genera el color del marcador en base a la calidad del aire
 *	retorna el color del marcador
 */
function obtenerColor(lecturas){
	// Hacer la comprobacion con base al índice IMECA
	var color='ee6e73';
	return color;
}

/*
 *	Funcion que genera el estilo de la capa de las estaciones
 *	retorna la declaracion del estilo
 */
function markerStyle(color){
	try{
		var marcador=makeMarker(color);
		var estilo = new ol.style.Style({
				image: new ol.style.Icon({
						img:marcador,
						imgSize:[30,30]
					})
			});
		return estilo;
	}catch(e){
		console.error('Este es el error: '+e);
	}
}

/*
 *	Funcion que formatea el marcador SGV para su visualizacion en el mapa
 *	retorna el marcador formateado
 */
function makeMarker(fill){
	var marcador=svgDeclaration(fill);
	var svgmarker = new Image();
	svgmarker.src='data:image/svg+xml,' + escape(marcador);
	return svgmarker;
}

/*
 *	Declaración del marcador del mapa hecho en SVG
 *	return svg declarado para su vista en el mapa
 */
function svgDeclaration(fill){
	return '<svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" height="30px" width="30px" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" viewBox="0 0 82.055138 80.91805">'+
		'<g transform="translate(-5.2269 -6.1517)">'+
		'<path d="m52.007 6.2685a35.158 35.158 0 0 0 -24.861 10.331l-0.0592-0.0547c-11.35 10.164-21.671 48.582-21.743 70.403 24.38 0.21743 59.66-5.7042 73.303-22.579l-0.0119-0.0109a35.158 35.158 0 0 0 8.5305 -22.931 35.158 35.158 0 0 0 -35.158 -35.158zm12.5 15.603c1.9675 0 3.7225 0.4254 5.2642 1.2757 1.5417 0.8354 2.7234 2.0214 3.5456 3.5579 0.83692 1.5366 1.2557 3.2822 1.2557 5.2364h-6.4534c0-1.5216-0.32305-2.678-0.9691-3.4686-0.63136-0.80558-1.5488-1.2083-2.7528-1.2083-1.1306 0-2.0117 0.34301-2.643 1.0292-0.63136 0.67131-0.94723 1.5814-0.94723 2.73 0 0.89508 0.35271 1.7079 1.0575 2.4389 0.70478 0.71607 1.9525 1.4623 3.7438 2.238 3.1274 1.1487 5.396 2.5582 6.8056 4.229 1.4242 1.6708 2.1364 3.7965 2.1364 6.3773 0 2.8344-0.88828 5.0499-2.6649 6.6461s-4.1921 2.3943-7.2461 2.3943c-2.0703 0-3.9569-0.43236-5.6601-1.2976s-3.0394-2.1035-4.0085-3.7146c-0.95439-1.6111-1.4316-3.5132-1.4316-5.7061h6.4971c0 1.8797 0.35989 3.2448 1.0794 4.0951 0.71946 0.85033 1.894 1.2753 3.5238 1.2753 2.2612 0 3.3921-1.2156 3.3921-3.6472 0-1.3277-0.33789-2.3276-1.0133-2.9989-0.66073-0.68623-1.8721-1.3945-3.634-2.1255-3.2156-1.2382-5.5281-2.6852-6.9377-4.3411-1.4096-1.6708-2.1141-3.64-2.1141-5.9075 0-2.7449 0.95432-4.9455 2.8631-6.6014 1.9234-1.6708 4.3605-2.5064 7.3117-2.5064zm-29.05 0.44787h5.9244l10.087 32.581h-6.9154l-1.7181-6.6684h-8.8317l-1.7177 6.6684h-6.8498l10.021-32.581zm2.951 8.8162-2.9952 11.614h5.9905l-2.9952-11.614z" fill-rule="evenodd" stroke="#000" stroke-width="1px" fill="#'+fill+'"/>'+
		'</g>'+
		'</svg>';
}

/*
 *	Funcion que se activa al hacer click en el mapa
 *	no retorna nada
 */
function clickOnMap(map){	
	map.on("click", function(e) {
		map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
			var obj=feature.getProperties();
			var popupcontent = setContentPopup(obj);
			header.innerHTML = popupcontent.headercontent;
			content.innerHTML = popupcontent.containercontent;
			var lecturas=$('#lecturas');
			closePanel($('#lectura'));
			overlay.setPosition(e.coordinate);
			lecturas.click(function(){
				if(!lecturas.hasClass('disabled')){
					setContentPanel(obj);
				}
			});
		});
	});
}

/*
 *	Contenido del popup asociado al overlay del mapa
 *	retorna un objeto con el conteniod del popup
 */
function setContentPopup(obj){
	try{
		if(obj.temperatura){
			return {'headercontent':'<span class="centrado"><h4>Estación '+obj.nombre.toUpperCase()+'</h4></span>',
					'containercontent':'<p>Temperatura: '+obj.temperatura+'°C</p><p>Humedad: '+obj.humedad+'% RH</p><div id="lecturas" class="btn">Ver lecturas</div>'
				}
		}else{
			return {'headercontent':'<span class="centrado"><h4>Estación '+obj.nombre.toUpperCase()+'</h4></span>',
				'containercontent':'<p>No se encuentra datos de asociados a esta estación*</p><p><code>*Probablemente sea una estación nueva</code></p><div class="btn disabled">Ver lecturas</div>'
			}
		}
	}catch(e){
		console.error(e);
	}
}

/*
 *	Objeto que contiene la traducción del Nombre de los sensores
 *	además de su unidad de medida
 *	Retorna un objeto
 */
function getNombreSensores(){
	return {'indices':{'temperatura':'Temperatura',
				'humedad':'Humedad Relativa',
				'co2':'Dióxido de Carbono',
				'co':'Monóxido de Carbono',
				'o3':'Ozono',
				'pm2_5':'Partículas > 2.5 µm',
				'so2':'Dióxido de Azufre',
				'no2':'Dióxido de Nitrógeno',
				'luz':'Luz',
				'pm10':'Partículas > 10 µm'},
			'simbolo':{'temperatura':'°C',
				'humedad':'%',
				'co2':'ppm',
				'co':'ppm',
				'o3':'ppm',
				'pm2_5':'µg/m3',
				'so2':'ppm',
				'no2':'ppm',
				'luz':'lux',
				'pm10':'µg/m3'}
		}
}

/*
 *	Funcion para eliminar propiedades indeseables del Feature Seleccionado
 *	Devuelve el objeto sin las propiedades indeseadas
 */
function deleteProperties(objeto){
	var prop ={
		'id':'id',
		'length':'length',
		'name':'name',
		'geometry':'geometry',
		'nombre':'nombre',
		'lon':'lon',
		'lat':'lat'
	};
	Object.keys(prop).forEach(function(key){
		delete objeto[key];
	});
	return true;
}

/*
 *	
 *
 */
function cloneObject(obj){
	var newObject = [];
	Object.keys(obj).forEach(function(key){
		newObject[key]=obj[key];
	});
	return newObject;
}

/*
 *	Contenido del panel inferior para mostrar mas información
 *	Retorna true porque el contenido del panel se añade
 */
function setContentPanel(obj){
	var lectura=$('#lectura');
	lectura.addClass('contentmax').removeClass('contentminchart').removeClass('contentmin').removeClass('contentmaxchart').removeClass('contentchartmin');
	var nomSensores = getNombreSensores();
	var lecturasSensores = cloneObject(obj);
	deleteProperties(lecturasSensores);
	var i=0;
	var sensores = '<div id="nombre" class="headerlectura">'+obj.nombre.charAt(0).toUpperCase() + obj.nombre.slice(1)+'</div>'+
		'<div class="row lecsensores"><div class="scrolling">';
	Object.keys(lecturasSensores).forEach(function(key){
		if(i==0){
			sensores +='<div class="col s1 offset-s1 centrado"><p class="nomsensores">'+nomSensores.indices[key]+'</p><p>'+lecturasSensores[key]+' '+nomSensores.simbolo[key]+'</p></div>';
		}else{
			sensores +='<div class="col s1 centrado"><p class="nomsensores">'+nomSensores.indices[key]+'</p><p>'+lecturasSensores[key]+' '+nomSensores.simbolo[key]+'</p></div>';
		}
		i++;
	});
	sensores+='</div></div><div style="text-align:center"><div class="hr"></div></div><div id="chartPanel" class="chartpanel"></div>';
	lectura.html(sensores);
	obtenerFechas(obj.nombre).then(function(r){
		var fechas=formatingDates(r);
		setChartPanel(obj.nombre,nomSensores.indices,fechas);
	}).catch(function(e){console.error(e);});
	return true;
}

/*
 *	Funcion que añade la lectura de los Sensores al feature
 *	retorna verdadero, porque el objeto ya contiene las propiedades deseadas
 */
function featureSetProperties(feature,propiedades){
	feature.setProperties(propiedades);
	return true;
}

/*
 *	
 *	
 */
function setChartPanel(estacion,nomSensores,fechas){
	var header=$('#nombre');
	header.click(function(){
		var divlectura=$('#lectura');
		if(divlectura.hasClass('contentmax')){
			divlectura.removeClass('contentmax').addClass('contentmaxchart');
		}else{
			divlectura.toggleClass('contentmaxchart contentchartmin');
		}
		$('#chartPanel').html(chartPanelContent(nomSensores));
		initializeElements(fechas);
		onSetDate();
	});
}

/*
 *	
 *	
 */
function chartPanelContent(nomSensores){
	var fecha=new Date();
	var htmlchartpanel='<div class="row chartarea"><div class="col s12"><div class="input-field col s5"><label for="senselect" class="active">Sensor</label><select id="senselect" onChange="graficarHist()">';
	Object.keys(nomSensores).forEach(function(key){htmlchartpanel+='<option value="'+key+'">'+nomSensores[key]+'</option>';});
	htmlchartpanel+='</select></div><div class="col s5 offset"><label for="datepicker" class="active">Fecha</label>'+
	'<input id="datepicker" type="date" class="datepicker" data-value="'+fecha.getFullYear()+'/'+fecha.getMonth()+'/'+fecha.getDate()+'"></div></div></div>'+
	'<div class="row s12"><div id="chartdiv" class="ct-chart"></div></div>';
	$('#senselect').on('chang');
	return htmlchartpanel;
}

/*
 *	Configuraciones del datepicker de materialize
 *	Hace falta cambiar las etiquetas de los dias de la semana al español
 *	Aquí se tendrá que recibir el arreglo de los dias que si tiene dato la estacion en cuestion
 */ 
function initializeElements(fechas){
	var $input=$('.datepicker').pickadate({
			monthsFull : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
			weekdaysFull : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
			weekdaysShort : ['D','L','M','M','J','V','S'],
			today: 'Hoy',
			clear: '',
			close: '✖',
			selectMonths: true,
			selectYears: 2,
			closeOnSelect: true,
			labelMonthNext: 'Próximo Mes',
			labelMonthPrev: 'Mes Anterior',
			labelMonthSelect: 'Seleccione un mes',
			labelYearSelect: 'Seleccione un año',
			disable:fechas,
			onStart: function(){onSetDate(this,this.get('select').pick)},
			onSet: function(thingSet) {onSetDate(this,thingSet.select)}
		});
	$('#senselect').material_select();
	return $input.pickadate('picker');
}

/*
 *	
 *
 */
function onSetDate(picka='',date){
	if(date!=undefined){
		obtenerHistorico(document.querySelector('.headerlectura').textContent.toLowerCase(),date).then(function(response){
			setDataHist(response);
			graficarHist(getDataHist());
		}).catch(function(error){console.error(error);});
		if(picka!=''){
			picka.close();	
		}
	}
}

/*
 *	
 *
 */
function closePanel(lectura){
	overlay.setPosition(undefined);
	if(lectura.hasClass('contentmaxchart')){
		lectura.removeClass('contentmaxchart').addClass('contentminchart');
	}else if(lectura.hasClass('contentchartmin')){
		lectura.removeClass('contentchartmin').addClass('contentmin');
	}else{
		lectura.removeClass('contentmax').addClass('contentmin');
	}
	return true;
}

/*
 *	
 *
 */
function obtenerHistorico(estacion,fecha=new Date().getTime()){
	return new Promise(function(resolve,reject){
		var resp=new XMLHttpRequest(); 
		resp.open('GET','historico?n='+estacion+'&date='+fecha);
		resp.onload=function(){
			if(!resp.status==200){
				reject(Error(resp.statusText));
			}else{
				resolve(JSON.parse(resp.responseText));
			}	
		};
		resp.onerror=function(){
			reject(Error('Error de Red!'));
		};
		resp.send();
	});
}

/*
 *
 *
 */
function obtenerFechas(estacion) {
	return new Promise(function (res,rej) {
		var r=new XMLHttpRequest();
		r.open('GET','dcd?e='+estacion);
		r.onload=function(){
			if(!r==200){
				rej(Error('Error'+r.statusText));
			}else{
				res(JSON.parse(r.responseText));
			}
		}
		r.send();
	})
}

function formatingDates(f){
	var ot=[],ofe=[],h;
	for(var i=0;i<f.length;i++)Object.keys(f[i]).forEach(function(key){ot[i]=f[i][key].split(',');});
	ofe[0]=true;
	for(var i=0;i<ot.length;i++){
		ofe[i+1]=[];
		Object.keys(ot[i]).forEach(function(key){
			if(key==1){
				ofe[i+1][key]=parseInt(ot[i][key])-1;
			}else{
				ofe[i+1][key]=parseInt(ot[i][key]);
			}
		});
	}
	return ofe;
}

/*
 *
 *
 */
function graficarHist(datos = getDataHist(),itemSelected = document.getElementById('senselect').value,itemNameSelected = document.getElementById('senselect').options[document.getElementById('senselect').selectedIndex].text,divGrafica = document.getElementById('chartdiv')){
	var valores=formatearDatosGrafica(datos,itemSelected);
	var etsnform=formatearDatosGrafica(datos,'created_at');
	var etiquetas=formatearFechas(etsnform);
	var chart;
	chart=new Chartist.Line(divGrafica,{
		labels:etiquetas,
		series:[valores],
	},{
		low:0,
		fullWidth:true,
		width:'95%',
		height:'13em',
		showArea:true,
		plugins: [
			Chartist.plugins.tooltip({
				currency: itemNameSelected+': ',
				appendToBody:true
			})
		]
	});
	animateChart(chart);
}

function formatearDatosGrafica(datos=getDataHist(),itemSelected){
	var objSensor = [];
	for(var i=0; i<datos.length;i++){
		Object.keys(datos[i]).forEach( function(key) {
			if(key==itemSelected){
				objSensor[i]=datos[i][key];
			}
		});
	}
	return objSensor;
}

function formatearFechas(fechas){
	var newObj=[];
	Object.keys(fechas).forEach(function(key){
		newObj[key]=fechas[key].substring(11,fechas[key].length-3);
	});
	return newObj;
}

function animateChart(chart){
	var seq = 0,
	  delays = 5,
	  durations = 500;

	chart.on('created', function() {
	  seq = 0;
	});

	chart.on('draw', function(data) {
	  seq++;

	  if(data.type === 'line') {
	    data.element.animate({
	      opacity: {
	        begin: seq * delays + 1000,
	        dur: durations,
	        from: 0,
	        to: 1
	      }
	    });
	  } else if(data.type === 'label' && data.axis === 'x') {
	    data.element.animate({
	      y: {
	        begin: seq * delays,
	        dur: durations,
	        from: data.y + 100,
	        to: data.y,
	        easing: 'easeOutQuart'
	      }
	    });
	  } else if(data.type === 'label' && data.axis === 'y') {
	    data.element.animate({
	      x: {
	        begin: seq * delays,
	        dur: durations,
	        from: data.x - 100,
	        to: data.x,
	        easing: 'easeOutQuart'
	      }
	    });
	  } else if(data.type === 'point') {
	    data.element.animate({
	      x1: {
	        begin: seq * delays,
	        dur: durations,
	        from: data.x - 10,
	        to: data.x,
	        easing: 'easeOutQuart'
	      },
	      x2: {
	        begin: seq * delays,
	        dur: durations,
	        from: data.x - 10,
	        to: data.x,
	        easing: 'easeOutQuart'
	      },
	      opacity: {
	        begin: seq * delays,
	        dur: durations,
	        from: 0,
	        to: 1,
	        easing: 'easeOutQuart'
	      }
	    });
	  } else if(data.type === 'grid') {
	    var pos1Animation = {
	      begin: seq * delays,
	      dur: durations,
	      from: data[data.axis.units.pos + '1'] - 30,
	      to: data[data.axis.units.pos + '1'],
	      easing: 'easeOutQuart'
	    };

	    var pos2Animation = {
	      begin: seq * delays,
	      dur: durations,
	      from: data[data.axis.units.pos + '2'] - 100,
	      to: data[data.axis.units.pos + '2'],
	      easing: 'easeOutQuart'
	    };

	    var animations = {};
	    animations[data.axis.units.pos + '1'] = pos1Animation;
	    animations[data.axis.units.pos + '2'] = pos2Animation;
	    animations['opacity'] = {
	      begin: seq * delays,
	      dur: durations,
	      from: 0,
	      to: 1,
	      easing: 'easeOutQuart'
	    };

	    data.element.animate(animations);
	  }
	});
}