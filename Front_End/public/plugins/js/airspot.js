/*
 *	Archivo que se encarga de establecer comunicacion con el backend del servidor de AirSpot
 *	Créditos: SIGPOT S.A. de C.V.
 *	Author: SIGPOT S.A. de C.V.
 */

/*
 *	Funciones SET GET p
 *
 */
 function setDataHist(response){
 	window.historico=response;
 	return historico;
 }
 function getDataHist(){
 	return historico;
 }

/*
 *	
 *
 */
function obtenerHistorico(estacion,fecha){
	return new Promise(function(resolve,reject){
		var resp=new XMLHttpRequest(); 
		resp.open('GET','historico?n='+estacion+'&date='+fecha);
		resp.onload=function(){
			if(resp.status===200){
				resolve(JSON.parse(resp.responseText));
			}else{
				reject(Error(resp.statusText));
			}	
		};
		resp.onerror=function(){
			reject(Error('Error de Red!'));
		};
		resp.send();
	});
}